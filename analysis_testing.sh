# Performs a list of analysis unit tests
# and reports success/failure.
# Should be sourced into concrete simulation nightly tasks

# TODO This needs to be refined to make it per analysis
for t in ${ANATESTLIST:-MCHistograms Efficiency Validation PIDFull EventTrackQA WeakDecayTutorial PWGMMMFT PWGMMFwdVertexing CheckDataModelMC}; do
  ${O2DPG_ROOT}/MC/analysis_testing/analysis_test.sh ${t}
  RC=$?
  if [ ! "${RC}" -eq 0 ]; then
    logs=$(get_error_logs $(pwd) --include-grep "${t}")
    send_mattermost "--text Analysis task ${task}:${t} **failed**. Return code ${RC} :x: --files ${logs}"
  else
    send_mattermost "--text Analysis task ${task}:${t} **passed** :white_check_mark:"
    compare_output_analyses $(pwd) ${task} ${t}
  fi
done
