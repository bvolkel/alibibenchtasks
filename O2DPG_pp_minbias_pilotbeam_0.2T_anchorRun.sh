#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias 
# production, targetting test beam conditions.
#
# In addition, we target to exercise the whole chain of anchoring
# mechanisms, including
# - transfer settings from DATA reconstruction pass scripts
# - anchor to the time of a specific data dating run, so that
#   correct CCDB are fetched
# - apply additional settings like vertex/beam spot etc., not yet coming
#   from elsewhere

[ ! ${ALIBI_EXECUTOR_FRAMEWORK} ] && export ALIBI_EXECUTOR_FRAMEWORK=ON

NTIMEFRAMES=2 ${O2DPG_ROOT}/MC/run/ANCHOR/2021/OCT/pass4/anchorMC.sh
