#!/bin/bash

###########################################
# Task performing a geometry overlap check
###########################################

today=`date +%d-%m-%Y-%H:%M`
TARGETDIR=geomoverlapcheck_${today}_ALIDIST:${ALIPERF_ALIDISTCOMMIT}_O2:${ALIPERF_O2COMMIT}
mkdir ${TARGETDIR}

if [[ -d "${TARGETDIR}" ]]
then
  cd ${TARGETDIR}

  # launch the job
  # --> create the geometry
  o2-sim-serial -n 0
  [ ! $? -eq 0 ] && return 1

  root -q -b -l ${O2_ROOT}/share/macro/checkGeomOverlaps.C\(\"o2sim_geometry.root\",0.01\) > overlapcheck.log 2>&1 
  grep "Overlap ov" overlapcheck.log > alloverlaps.dat

  eos mkdir /eos/user/a/aliperf/simulation/overlapchecks/${TARGETDIR}
  eos cp * /eos/user/a/aliperf/simulation/overlapchecks/${TARGETDIR}

  send_mattermost "--text Here are the overlaps --files alloverlaps.dat"

  value=$(wc alloverlaps.dat | awk '//{print $1}')

  # upload result to InfluxDB
  METRIC="geomoverlaps,host=alibicompute01.cern.ch,alidist=${ALIPERF_ALIDISTCOMMIT},o2=${ALIPERF_O2COMMIT} value=${value}"
  echo "${METRIC}" > metrics.dat

  # send it to Influx (use framework function)
  submit_to_AliPerf_InfluxDB metrics.dat
fi
