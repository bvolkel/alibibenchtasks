#!/bin/bash                                                                                                                                                                               

# This is a very simple check if the latest CVMFS installation is ok                                                                                                                      
# It just loads O2 and executes a small simulation.                                                                                                                                       

# version to check (the last nightly)                                                                                                                                                                        
O2_PACKAGE_LATEST=`find /cvmfs/alice.cern.ch/el7-x86_64/Modules/modulefiles/O2 -name "night*" -type f -printf "%f\n" | tail -n1`

# create command                                                                                                                                                                          
COMMAND="export; eval \"\$(/cvmfs/alice.cern.ch/bin/alienv printenv O2::\"${O2_PACKAGE_LATEST}\")\""
COMMAND="${COMMAND}; o2-sim-serial -n 1 -g pythia8pp --skipModules ZDC -e TGeant3 -j 1 --seed 1 &> log"

# execute in a shubshell with clean environment (so as not to interfere with already loaded environment)                                                                                  
env - bash -c "${COMMAND}"

# analyse return code                                                                                                                                                                     
RETURN_CODE=$?
[ ! "${RETURN_CODE}" -eq 0 ] && echo "test failed"

# return with this erro code (or exit)                                                                                                                                                    
return ${RETURN_CODE} 2>/dev/null || exit ${RETURN_CODE}
