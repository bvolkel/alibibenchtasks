#!/bin/bash

#
# This is running an async reco setup by utilizing part of the anchoring MC 
# script/setup

[ ! ${ALIBI_EXECUTOR_FRAMEWORK} ] && export ALIBI_EXECUTOR_FRAMEWORK=ON

# get a CTF input_file
export CTF_TEST_FILE="o2_ctf_run00505673_orbit0000035596_tf0000000037.root"
alien.py cp /alice/data/2021/OCT/505673/raw/0850/${CTF_TEST_FILE} file:./

# run the async reco (pass4) and switch off MC
NO_MC=ON NTIMEFRAMES=2 ${O2DPG_ROOT}/MC/run/ANCHOR/2021/OCT/pass4/anchorMC.sh
