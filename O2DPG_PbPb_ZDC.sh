#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple PbPb production
# including ZDC

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# ----------- SETUP LOCAL CCDB CACHE --------------------------
export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb

# ----------- START ACTUAL JOB  -----------------------------

NWORKERS=${NWORKERS:-16}
SIMENGINE=${SIMENGINE:-TGeant4}

# create workflow
${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 5020 -col PbPb -gen pythia8 -proc "heavy_ion" -tf 2 \
                                                     -ns 10 -e ${SIMENGINE} -j ${NWORKERS}          \
                                                     --include-qc --include-analysis --with-ZDC     \
                                                     -run 310000 -seed 624

send_mattermost "--text **with ZDC**"
# run workflow
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod
MCRC=$(sim_passed $? $(pwd))

if [ "${MCRC}" = "0" ]; then

  unset ALICEO2_CCDB_LOCALCACHE
  # perform some analysis testing
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  . ${DIR}/analysis_testing.sh
fi

return ${MCRC} 2> /dev/null || exit ${MCRC}
