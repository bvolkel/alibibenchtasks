#!/bin/bash

#
# A example workflow MC->RECO->AOD for a simple pp min bias
# production, targetting test beam conditions.

# make sure O2DPG + O2 is loaded
[ ! "${O2DPG_ROOT}" ] && echo "Error: This needs O2DPG loaded" && exit 1
[ ! "${O2_ROOT}" ] && echo "Error: This needs O2 loaded" && exit 1

# ----------- START ACTUAL JOB  -----------------------------

NWORKERS=${NWORKERS:-16}
MODULES="--skipModules ZDC"
SIMENGINE=${SIMENGINE:-TGeant4}

export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb

# create workflow
# with a low interaction rate, the number of signals per tf is low (~11ms timeframe)
${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 900 -col pp -gen pythia8 -proc inel -tf 25     \
                                                        -ns 110 -e ${SIMENGINE}                \
                                                        -j ${NWORKERS} -interactionRate 10000  \
                                                        -confKey "Diamond.width[2]=6" --include-qc \
                                                        --include-analysis --with-ZDC          \
                                                        -productionTag "alibi_O2DPG_pp_minbias_testbeam" \
                                                        -run 301000 -seed 624

# run workflow
# allow increased timeframe parallelism with --cpu-limit 32
send_mattermost "--text **with ZDC**"
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -tt aod --cpu-limit 32
MCRC=$(sim_passed $? $(pwd))  # <--- we'll report back this code

if [ "${MCRC}" = "0" ]; then
  # publish the AODs to ALIEN
  copy_ALIEN "*AO2D*"

  # do QC tasks
  ${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json --target-labels QC --cpu-limit 32
  RC=$?
  err_logs=$(get_error_logs $(pwd) --include-grep "QC")
  [ ! "${RC}" -eq 0 ] && send_mattermost "--text QC stage **failed** :x: --files ${err_logs}" || send_mattermost "--text QC **passed** :white_check_mark:"

  unset ALICEO2_CCDB_LOCALCACHE
  # perform some analysis testing
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  . ${DIR}/analysis_testing.sh
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* QC* pipeline*
copy_ALIEN mcarchive.tar.gz

return ${MCRC} 2>/dev/null || exit ${MCRC}
