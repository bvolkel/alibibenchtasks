# Performing a PWGDQ pp embedding example for the pilot beam
# Embedding is done into each background event 1-1

RNDSEED=${RNDSEED:-0}
NSIGEVENTS=${NSIGEVENTS:-110}
NBKGEVENTS=${NBKGEVENTS:-110}
NWORKERS=${NWORKERS:-8}
NTIMEFRAMES=${NTIMEFRAMES:-25}

# ----------- SETUP LOCAL CCDB CACHE --------------------------
export ALICEO2_CCDB_LOCALCACHE=$PWD/.ccdb


${O2DPG_ROOT}/MC/bin/o2dpg_sim_workflow.py -eCM 900 -gen external -j ${NWORKERS} -ns ${NSIGEVENTS} -tf ${NTIMEFRAMES} -e TGeant4 \
    -mod "--skipModules ZDC" \
    -confKey "GeneratorExternal.fileName=${O2DPG_ROOT}/MC/config/PWGDQ/external/generator/GeneratorParamPromptJpsiToElectronEvtGen_pp13TeV.C;GeneratorExternal.funcName=GeneratorParamPromptJpsiToElectronEvtGen_pp13TeV();Diamond.width[2]=6"         \
    -genBkg pythia8 -procBkg cdiff -colBkg pp --embedding -nb ${NBKGEVENTS} \
    -interactionRate 10000                                                 \
    -confKeyBkg "Diamond.width[2]=6" --include-analysis                    \
    -productionTag "alibi_O2DPG_PWGDQ_ppJpsi_pilotbeam" -run 301000 -seed 624

export FAIRMQ_IPC_PREFIX=./

# run workflow (highly-parallel)
${O2DPG_ROOT}/MC/bin/o2_dpg_workflow_runner.py -f workflow.json -t aod --cpu-limit 32

MCRC=$(sim_passed $? $(pwd))

if [ "${MCRC}" = "0" ]; then
  # publish the AODs to ALIEN
  copy_ALIEN "*AO2D*"

  unset ALICEO2_CCDB_LOCALCACHE
  # perform some analysis testing
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  . ${DIR}/analysis_testing.sh
fi

# publish the original data to ALIEN
tar -czf mcarchive.tar.gz workflow.json tf* pipeline*
copy_ALIEN mcarchive.tar.gz

unset FAIRMQ_IPC_PREFIX

return ${MCRC} 2> /dev/null || exit ${MCRC}
