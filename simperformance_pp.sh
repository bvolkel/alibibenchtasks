#!/bin/bash

DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
echo "${DIR}"

NEVENTS=100 SYSTEM=pp GEN=pythia8pp . ${DIR}/simperformance_template.sh
