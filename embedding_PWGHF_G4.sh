# performing the PWGHF embedding example with G4 and 10 timeframes

cp ${O2DPG_ROOT}/MC/run/PWGHF/embedding_benchmark.sh .
SEED=624 NSIGEVENTS=100 NBKGEVENTS=100 NTIMEFRAMES=10 SIMENGINE=TGeant4 NWORKERS=8 JOBUTILS_SKIPDONE=ON JOBUTILS_KEEPJOBSCRIPT=ON JOBUTILS_MONITORMEM=ON ./embedding_benchmark.sh

MCRC=$(sim_passed $? $(pwd))

if [ "${MCRC}" = "0" ]; then
  # publish the AODs to ALIEN
  copy_ALIEN "*AO2D*"

  # perform some analysis testing
  DIR="$( cd "$( dirname "${BASH_SOURCE[0]}" )" >/dev/null 2>&1 && pwd )"
  . ${DIR}/analysis_testing.sh
fi

return ${MCRC} 2> /dev/null || exit ${MCRC}
